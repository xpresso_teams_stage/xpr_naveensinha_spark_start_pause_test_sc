__author__ = 'Naveen'

import os
import sys
import time

if os.path.exists('packs.zip'):
    sys.path.insert(0, 'packs.zip')

try:
    import pyspark
except:
    import findspark

    findspark.init()
    import pyspark

from pyspark.sql import SparkSession
from pyspark.ml import Pipeline
from pyspark.shell import sc
from pyspark.sql.types import StructField, StructType, StringType

from counter1.app.counter import Counter as Counter1
from counter2.app.counter import Counter as Counter2
from counter3.app.counter import Counter as Counter3

if __name__ == "__main__":
    ts = str(int(time.time()))

    spark = SparkSession \
        .builder \
        .appName("pipeline-name" + ts) \
        .getOrCreate()

    print("Creating Pipeline from the counters")
    counter1 = Counter1("counter_1")
    counter2 = Counter2("counter_2")
    counter3 = Counter3("counter_3")
    print("Creating pipeline stages ")
    pipeline_stages = [counter1, counter2, counter3]
    pipeline = Pipeline(stages=pipeline_stages)

    print("Starting pipeline")
    field = [StructField("CounterCount", StringType(), True)]
    schema = StructType(field)
    df = spark.createDataFrame(sc.emptyRDD(), schema=schema)
    pipelineModel = pipeline.fit(df)
    df = pipelineModel.transform(df)
    print("Pipeline completed")
    spark.stop()
