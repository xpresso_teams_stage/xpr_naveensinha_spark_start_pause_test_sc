from .counter import Counter
import sys


def main():
    counter = Counter(sys.argv[1])
    counter.start(sys.argv[2:])


if __name__ == "__main__":
    main()
