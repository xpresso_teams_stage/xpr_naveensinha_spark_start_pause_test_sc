import threading
import time
import os
from datetime import datetime

from .controller_stub import ControllerStub


class AbstractPipelineComponent:
    def __init__(self):
        print("Initializing component")
        # note run ID
        self.run_id = None
        # set run_status
        self.run_status = "IDLE"
        # state of componet (to be saved on pause, and loaded on restart)
        self.state = None
        # status of component (to be reported on periodic basis - consists of status dict and metrics dict)
        self.status = None
        # output of component (to be stored on disk on completion)
        self.output = None
        # final results of component (to be stored in database on completion)
        self.results = None

        self.run_status_thread = None
        self.is_thread_needed = True
        self.controller = ControllerStub()
        print("Component initlaized")

    def start(self, *args):
        print("Parent component starting", flush=True)
        self.run_id = args[0]
        # check status immediately and take action if required
        self.get_run_status()

        # start thread to check run status
        self.run_status_thread = threading.Thread(target=self.check_run_status)
        self.is_thread_needed = True
        self.run_status_thread.start()
        self.controller.pipeline_component_started(self.run_id, self.name)

    def terminate(self):
        print("Parent component terminating", flush=True)
        self.controller.pipeline_component_terminated(self.run_id, self.name)
        self.is_thread_needed = False
        os._exit(0)

    def terminate_without_exit(self):
        print("Parent component terminating without exit", flush=True)
        self.controller.pipeline_component_terminated(self.run_id, self.name)
        self.is_thread_needed = False

    def pause(self):
        print("Parent component saving state and exiting", flush=True)
        self.controller.pipeline_component_paused(self.run_id, self.name,
                                                  self.state)
        self.is_thread_needed = False
        os._exit(0)

    def restart(self):
        print("Parent component restarting", flush=True)
        state = self.controller.pipeline_component_restarted(self.run_id,
                                                             self.name)
        if state is not None:
            self.state = state
            self.controller.update_pipeline_run_status(self.run_id, "RUNNING")

        else:
            print(
                "This component already completed execution. Some other component needs to restart. Terminating",
                flush=True)
            self.terminate_without_exit()

    def completed(self):
        print("Parent component completed", flush=True)
        self.controller.pipeline_component_completed(self.run_id, self.name)
        self.is_thread_needed = False

    def report_pipeline_status(self, status):
        print("Parent component reporting status", flush=True)
        self.controller.report_pipeline_status(self.run_id, self.name, status)

    def check_run_status(self):
        while self.is_thread_needed:
            self.get_run_status()
            time.sleep(5)
        print("Stopping check run status thread")

    def get_run_status(self):
        self.run_status = self.controller.get_pipeline_run_status(self.run_id)
        print(
            "Run ID: {}, Time:{}, Component: {}, Status: {}".format(
                str(self.run_id),
                datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                self.name,
                self.run_status))
        print("RUN STATUS: {}".format(self.run_status), flush=True)
        if self.run_status == "IDLE":
            pass
        elif self.run_status == "RUNNING":
            pass
        elif self.run_status == "TERMINATED":
            print("Terminating component", flush=True)
            self.terminate()
        elif self.run_status == "PAUSED":
            print("Pausing component", flush=True)
            self.pause()
        elif self.run_status == "RESTARTED":
            print("Restarting component", flush=True)
            self.restart()
        else:
            print("No action required...continuing", flush=True)
