import sys
import time
import os

from pyspark.ml import Transformer

from .abstract_component import AbstractPipelineComponent


class Counter(Transformer, AbstractPipelineComponent):
    def _transform(self, dataset):
        args = ["20"]
        self.start(*args)

    def __init__(self, name):
        Transformer.__init__(self)
        AbstractPipelineComponent.__init__(self)
        self.name = name
        self.final_count = 0
        self.state = {"initial_count": 1}
        self.counter = 1

    def start(self, *args):
        # start super class
        AbstractPipelineComponent.start(self, *args)

        print("Child component starting", flush=True)

        # set parameters
        self.set_parameters(*args)

        # execute
        self.counter = self.state["initial_count"]
        print("Starting count from " + str(self.counter), flush=True)
        while self.counter <= self.final_count:
            print(self.counter, flush=True)
            self.report_pipeline_status(
                {"status": "Counted up to " + str(self.counter)})
            self.counter += 1
            time.sleep(1)
        self.completed()
        return

    def terminate(self):
        print("Child component cleaning up", flush=True)
        AbstractPipelineComponent.terminate(self)

    def terminate_without_exit(self):
        print("Child component cleaning up without exit", flush=True)
        AbstractPipelineComponent.terminate_without_exit(self)
        self.counter = self.final_count + 1

    def pause(self):
        print("Child component pausing", flush=True)
        self.state["initial_count"] = self.counter
        AbstractPipelineComponent.pause(self)

    def set_parameters(self, args):
        if len(args) < 2:
            print("Usage: PipelineComponent <run_id> <final_count>", flush=True)
            exit(-1)

        self.final_count = 30


def main():
    counter = Counter("counter1")
    counter.start(sys.argv[1:])


if __name__ == "__main__":
    main()
